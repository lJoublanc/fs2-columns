# fs2-columns

This is an extension to [fs2](https://github.com/functional-streams-for-scala/fs2).  It provides a new `Chunk` constructor that stores data column-wise. An example makes this clearer:

Typically, a `fs2.Chunk[Tuple2[Long,Float]]` would be backed by a chunk, backed by a boxed sequence of `Tuple2`'s.

```scala
scala> import fs2._
import fs2._

scala> val l = Chunk(1 to 10: _*)
  l: fs2.Chunk[Int] = Chunk(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

  scala> val r = l map (_.toDouble)
  r: fs2.Chunk[Double] = Chunk(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0)

  scala> l zip r
  res0: fs2.Chunk[(Int, Double)] = Chunk((1,1.0), (2,2.0), (3,3.0), (4,4.0), (5,5.0), (6,6.0), (7,7.0), (8,8.0), (9,9.0), (10,10.0))
```

This library allows you to instead use the constructor `|:` to zip together two chunks:

```scala
scala> import fs2.columns._
import fs2.columns._

scala> val x = l |: r
x: Int |: Double = Chunk((1,1.0), (2,2.0), (3,3.0), (4,4.0), (5,5.0), (6,6.0), (7,7.0), (8,8.0), (9,9.0), (10,10.0))
```

Note this is a '''right'''-associative operator, unlike the traditional `zip`.

It looks the same as the previous example, but it is in fact backed by two chunks, one of `Long` and one of `Float` that ideally would be arrays of primitives, [contiguous in memory][^1]

`|:` is also an extractor that allows accessing data column-wise. For instance:

```scala
Stream.chunk(x).mapChunks {
  case longsAndFloats => 
    Chunk(longsAndFloats.foldLeft(0.0){ case (nextr,(_,r)) => r + nextr })
  case longs |: floats =>
    Chunk(floats.foldLeft(0.0)(_ + _))
}
```

In the above example, we are summing the floats, column-wise. The two `case`s have the same result, but the second one is compiling down to a single `for` loop over a sequence of primitive floats, rather than looping over references to `Tuple2` and then dreferencing the second element.

There is a downside to all this, which is that if we use row-wise operations, e.g. if we replaced the above example with `s.map(...)`, then it is slower than iterating over a traditional `fs2.Chunk`.

[^1]: The JVM memory model does not guarantee memory allocations to be contiguous. However, using `java.nio`'s `mmap`'ed `ByteBuffers`, this can be achieved using external memory allocation.
