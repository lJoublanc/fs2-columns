package fs2

package object columns {

  /** Recursive chunk that allows preserving data in vectorized (column-wise)
    * form.
    *
    * The scheme used is similar to `shapeless.HList` but uses `scala.Tuple2`
    * for elements.  Also, there is no final `Nil` element, and therefore no
    * concept of an empty column.  This representation has been chosen for
    * forward-compatibility with Dotty.  It means that in future it will be
    * possible to pattern match this nested structure with a flat tuple-N.
    *
    * One unfortunate inconsistency is that most collection libraries providing
    * a `zip` operator have left-associative behaviour. Therefore you must be
    * careful when using such operators to ensure right-nested structure is
    * preserved.
    *
    * @param headCol  Leading columns.
    * @param tailCols Trailing columns. May itself be a `|:` to right-recurse.
    * @example {{{
    * scala> import fs2._ ; import fs2.columns._
    *
    * // This looks like a normal chunk in the type signature ...
    * scala> val x = Chunk(1 to 6: _*) |: Chunk('a' to 'f': _*) |: Chunk('A' to 'F': _*)
    * x: fs2.Chunk[(Int, (Char, Char))] = Chunk((1,(a,A)), (2,(b,B)), (3,(c,C)), (4,(d,D)), (5,(
    * e,E)), (6,(f,F)))
    *
    * // You can map over it as usual ...
    * scala> Stream chunk x map { case (i, (a, b)) => (a,b) } toVector
    * Vector[(Char, Char)] = Vector((a,A), (b,B), (c,C), (d,D), (e,E), (f,F))
    *
    * // But the `|:` extractor with `mapChunks` allows you to access the underlying columns.
    * scala> Stream chunk x mapChunks { case i |: a |: b => a |: b } toVector
    * Vector[(Char, Char)] = Vector((a,A), (b,B), (c,C), (d,D), (e,E), (f,F))
    *
    * }}}
    */
  case class |:[+O, +Os](headCol: Chunk[O], tailCols: Chunk[Os]) extends Chunk[(O,Os)] { self =>

    def apply(i: Int): (O, Os) = (headCol(i), tailCols(i))

    def copyToArray[O2 >: (O, Os)](xs: Array[O2], start: Int): Unit = 
      for { i <- 0 until size } xs(start + i) = apply(i)

    def size: Int = headCol.size

    protected def splitAtChunk_(n: Int): (Chunk[(O, Os)], Chunk[(O, Os)]) = {
      val (lHead,rHead) = headCol splitAt n
      val (lTail,rTail) = tailCols splitAt n
      (new |:(lHead,lTail), new |:(rHead,rTail))
    }

    /** Left-associative zip, which creates row-major chunks. */
    @deprecated("0.1.3","Consider using the right-associative |: operator instead, for Dotty compatibility.")
    override def zip[O2](right: Chunk[O2]): Chunk[((O,Os),O2)] = {
      val left = self
        if (left.size < right.size)
          new |:(left, right take left.size)
        else
          new |:(left take right.size, right)
    }

    //TODO : specialize to only zip up chunks bigger than a certain size.
    /** Right-associative zip, which creates column-major chunks. */
    def |:[O2](left : Chunk[O2]): Chunk[(O2,(O,Os))] = {
        val right = self
        if (left.size < right.size)
          new |:(left, right take left.size)
        else
          new |:(left take right.size, right)
    }
  }

  /** Sequences e.g. `Chunk[(A,(B,C))]` into `(Chunk[A],(Chunk[B], Chunk[C]))` */
  trait Sequence[A] {
    type Out
    def apply(column: Chunk[A]): Out
  }

  protected trait LowPrioSequence {
    implicit def tuple[A,B]: Sequence.Aux[(A,B),(Chunk[A],Chunk[B])] =
      new Sequence[(A,B)] {
        type Out = (Chunk[A],Chunk[B])
        def apply(column: Chunk[(A,B)]): (Chunk[A],Chunk[B]) =
          column match { 
            case as |: bs => (as,bs)
            case abs => abs.map(_._1) -> abs.map(_._2) //TODO: optimize
          }
      }
  }

  object Sequence extends LowPrioSequence {

    type Aux[C,T] = Sequence[C] { type Out = T }

    implicit def uncons[A,B <: (_,_),TB](implicit sequence: Sequence.Aux[B,TB]): Sequence.Aux[(A,B),(Chunk[A],TB)] =
      new Sequence[(A,B)] {
        type Out = (Chunk[A],TB)
        /** Sequences `Chunk[(A,(B,C))]` into `(Chunk[A],(Chunk[B], Chunk[C]))` */
        def apply(column: Chunk[(A,B)]): (Chunk[A],TB) =
          column match {
            case as |: bs => (as,sequence(bs))
            case abs => abs.map(_._1) -> sequence(abs.map(_._2)) //TODO: optimize
          }
      }
  }

  /** Unsequences e.g. `(Chunk[A],(Chunk[B], Chunk[C]))` into `Chunk[(A,(B,C))]` */
  trait Unsequence[A,Bs] {
    type Last
    type Out = Chunk[(A,Last)]
    def apply(tup: (Chunk[A],Bs)): Out
  }

  protected trait LowPrioUnsequence {

    implicit def tuple[A,B]: Unsequence.Aux[A,Chunk[B],B] =
      new Unsequence[A,Chunk[B]] {
        type Last = B
        def apply(tup: (Chunk[A], Chunk[B])): Chunk[(A,B)] = tup match {
          case (as,bs) => as |: bs
        }
      }
  }

  object Unsequence extends LowPrioUnsequence {

    type Aux[A, B, L] = Unsequence[A,B] { type Last = L }

    def apply[A,CB,Bs](tups: (Chunk[A],CB))(implicit 
      unsequence: Unsequence.Aux[A,CB,Bs]): unsequence.Out =
      unsequence(tups)

    implicit def uncons[A,B,Bs,TupBs](
        implicit tail: Unsequence.Aux[B,Bs,TupBs]):
      Unsequence.Aux[A,(Chunk[B],Bs),(B,tail.Last)] =
      new Unsequence[A,(Chunk[B],Bs)] {
        type Last = (B, tail.Last)
        def apply(tup: (Chunk[A],(Chunk[B],Bs))) = tup match { 
          case (ca, bb) => ca |: tail(bb)
        }
      }
  }

  implicit class ColumnSyntax[A](val right: Chunk[A]) extends AnyVal {
    /** Right-associative zip, which preserves columnar orientation of chunks. */
    def |:[B](left: Chunk[B]) = new |:(left, right) 
    def sequence(implicit seqn: Sequence[A]): seqn.Out = 
      seqn(right)
  }
}
