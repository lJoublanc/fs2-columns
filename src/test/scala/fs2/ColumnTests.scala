package fs2

import org.scalatest._
import org.scalatest.matchers._
import fs2.columns._

class ColumnTests extends PropSpec with Matchers {

  val i = Chunk.ints(1 to 5 toArray)

  val d = Chunk.doubles(1 to 5 map (_.toDouble) toArray)

  val id = Chunk.seq(i.toVector zip d.toVector)

  val idi = Chunk.seq(i.toVector zip (d.toVector zip i.toVector))

  property("Columns should sequence into tuples") {

    withClue("column-major") {
     (i |: d).sequence shouldEqual ((i, d))

     (i |: d |: i).sequence shouldEqual ((i, (d, i)))
    }

    withClue("row-major") {
      id.sequence shouldEqual ((i, d))

      idi.sequence shouldEqual ((i,(d, i)))
    }
  }

  property("Nested tuples should un-sequence into columns") {
    Unsequence((i, d)) shouldEqual (i |: d)
    Unsequence((i, (d, i))) shouldEqual (i |: d |: i)
    Unsequence((d, (i, (d, i)))) shouldEqual (d |: i |: d |: i)
  }
}
