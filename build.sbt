name := "fs2-columns"

description := "Columnwise operations for fs2-streams"

organization := "com.dinogroup"

crossScalaVersions := Seq("2.12.8", "2.13.0-M5")

scalaVersion := "2.13.0-M5"

enablePlugins(GitVersioning)

licenses += "MIT" -> url("http://opensource.org/licenses/MIT")

bintrayRepository := "dinogroup"

bintrayOrganization := Some("dmbl")

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-language:implicitConversions",
  "-language:higherKinds",
  "-language:reflectiveCalls",
  "-language:existentials",
  "-language:postfixOps")

libraryDependencies ++= Seq(
  "co.fs2" %% "fs2-core" % "1.0.3",
  "org.scalatest" %% "scalatest" % "3.0.6-SNAP5" % "test",
)

/* The below is needed for CI on Gitlab/Kubernetes.
 * As this is a library, it is not mean to be run independently,
 * therefore this is a null task.
 */

val stage = taskKey[Unit]("Stage task")

val Stage = config("stage")

stage := { }
